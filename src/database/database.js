import firebase from 'firebase/app';
import 'firebase/firestore';

firebase.initializeApp({
    apiKey: "AIzaSyAsu0NX2hU1nq5l60gcEkkrfyFbKgb0bpQ",
    authDomain: "tattoo-artists-portal.firebaseapp.com",
    databaseURL: "https://tattoo-artists-portal.firebaseio.com",
    projectId: "tattoo-artists-portal",
    storageBucket: "tattoo-artists-portal.appspot.com",
    messagingSenderId: "1096759120189",
    appId: "1:1096759120189:web:4a1b06cae732d5379ff554",
    measurementId: "G-M1F38KY9J8"
  });