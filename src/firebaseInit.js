import firebase from "firebase";
import "firebase/firestore";
import  firebaseConfig from "./firebaseConfig"
console.log("firebaseConfig", firebaseConfig)
const firebaseApp = firebase.initializeApp(firebaseConfig);
export default firebaseApp.firestore();