import Vue from 'vue';
import VueRouter from 'vue-router';
import Artists from '../components/Artists';
import ViewArtist from '../components/ViewArtist';

Vue.use(VueRouter)

const routes = [
  {
    path: '/artists',
    name: 'Artists',
    component: Artists,

  },
  {
    path: '/artist/:name',
    name: 'ViewArtist',
    component: ViewArtist
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" '../views/About.vue'*/ )
  }
]

const router = new VueRouter({
  routes
})

export default router
